<?php
declare(strict_types=1);


namespace RedirectTspu;


function init(array $options = []): void
{
    if (!isset($options['dns'])) {
        return;
    }

    if (stristr(Url::getQuery(), 'redirectcache=clear') == true) {

        $c = new Cache();

        Params::setCacheKey($options['dns']);
        $c->setCachePath(Params::getCachePath());

        if ($c->isCached(Params::getCacheKey())) {
            $c->erase(Params::getCacheKey());
        }
    }

    $client = ClientBuilder::create($options)->getClient();

    $client->redirect();
}

function dd($data = null)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    die;
}