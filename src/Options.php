<?php
declare(strict_types=1);

namespace RedirectTspu;


class Options
{
    public $options;
    private $cache;
    private $cachekey;
    private $cacheexpire;

    public function __construct(array $options = [])
    {
        $this->options = $options;

        Params::setCacheKey($this->options['dns']);

        $this->cachekey = Params::getCacheKey();
        $this->cacheexpire = Params::getCacheExpire();

        $this->options['url'] = Url::getUrl();
        $this->options['data'] = $this->getData();
    }

    private function setCache()
    {
        $this->cache = new Cache();
        $this->cache->setCachePath(Params::getCachePath());
        $this->cache->eraseExpired();
    }

    private function getData()
    {
        $this->setCache();
        $data = [];
        $is_cache = $this->cache->isCached($this->cachekey);

        if (!$is_cache || is_null($is_cache)) {
            $data = $this->request();
        } else {
            // get cached data by its key
            $cData = $this->cache->retrieve($this->cachekey);

            if ($cData) {
                $data = json_decode($cData);
            }
        }

        return $data;
    }

    private function request()
    {
        $data = [];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $this->options['dns'], [
            'timeout' => 2.0,
        ]);

        if ($response->getStatusCode() == 200) {
            $content = (string)$response->getBody();
            $data = json_decode($content);
        }

        if (count($data) == 0)
            return null;

        $this->cache->store($this->cachekey, $content, $this->cacheexpire);

        return $data;
    }
}