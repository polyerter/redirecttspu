<?php


namespace RedirectTspu;


class Url
{
    public static function getQuery()
    {
        return (!is_null($_SERVER['QUERY_STRING'])
            && isset($_SERVER['QUERY_STRING'])
            && !empty($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : '';
    }

    public static function getRequest()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public static function getHost()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public static function getUrl()
    {
        return (isset($_SERVER["HTTPS"]) ? 'https' : 'http') . "://" . self::getHost() . self::getRequest();
    }
}