<?php


namespace RedirectTspu;


class Params
{
    CONST EXPIRE = 60 * 10; //60 * 10 = 1- min

    private static $cachekey = 'redirect';

    public static function getCacheKey()
    {
        return self::$cachekey;
    }

    public static function setCacheKey(string $dns)
    {
        $parse = explode('/', $dns);

        self::$cachekey = end($parse);
    }

    public static function getCacheExpire()
    {
        return self::EXPIRE;
    }

    public static function getCachePath()
    {
        return dirname(__DIR__) . '/../../../cache/';
    }
}