<?php
declare(strict_types=1);


namespace RedirectTspu;


use RedirectTspu\Comparator\Context;


class Client
{
    private $options;

    public function __construct(Options $options)
    {
        $this->options = $options->options;
    }

    public function redirect($moved = false)
    {
        $url = $this->options['url'];

        if (isset($this->options['data']) && is_array($this->options['data']) && count($this->options['data']) > 0) {

            foreach ($this->options['data'] as $k => $item) {
                $class = $this->getNamespace($item->type);

                if (!class_exists($class)) {
                    return;
                }

                $obj = new Context(new $class($item, $url));

                if ($obj->execute()) {
                    $to = $obj->getToUrl();

                    // If the headers have been sent, then we cannot send an additional location header
                    // so we will output a javascript redirect statement.
                    if (headers_sent()) {
                        echo "<script>document.location.href='" . str_replace("'", "&apos;", $to) . "';</script>\n";
                    } else {
                        header($moved ? 'HTTP/1.1 301 Moved Permanently' : 'HTTP/1.1 303 See other');
                        header('Location: ' . $to);
                    }

                    $this->close();
                }
            }
        }
    }

    private function getNamespace($name)
    {
        return "RedirectTspu\Comparator\\" . $name;
    }

    private function close()
    {
        exit();
    }
}