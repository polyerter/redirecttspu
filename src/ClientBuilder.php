<?php
declare(strict_types=1);


namespace RedirectTspu;


class ClientBuilder
{
    private $options;

    public function __construct(Options $options = null)
    {
        $this->options = $options ?? new Options();
    }

    public static function create(array $options = [])
    {
        return new static(new Options($options));
    }

    public function getClient()
    {
        return new Client($this->options);
    }

    public function getOptions(): Options
    {
        return $this->options;
    }
}