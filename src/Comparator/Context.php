<?php
declare(strict_types=1);


namespace RedirectTspu\Comparator;


class Context
{
    private $comparator;

    public function __construct(AbstractComparator $comparator)
    {
        $this->comparator = $comparator;
    }

    public function execute(): bool
    {
        return $this->comparator->compare();
    }

    public function getToUrl()
    {
        return $this->comparator->getToUrl();
    }

}