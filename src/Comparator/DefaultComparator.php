<?php
declare(strict_types=1);


namespace RedirectTspu\Comparator;


class DefaultComparator extends AbstractComparator
{
    public function compare(): bool
    {
        return $this->item->from == $this->url ? true : false;
    }
}