<?php
declare(strict_types=1);


namespace RedirectTspu\Comparator;


use function RedirectTspu\dd;

abstract class AbstractComparator
{
    protected $url = null;
    protected $item = null;

    public function __construct($item, string $url)
    {
        $this->url = $url;
        $this->item = $item;
    }

    /**
     * @param string $url
     * @param string $pattern
     *
     * @return bool
     */
    abstract public function compare(): bool;

    public function getToUrl()
    {
        return $this->item->to;
    }
}