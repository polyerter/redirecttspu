<?php
declare(strict_types=1);


namespace RedirectTspu\Comparator;


use function RedirectTspu\dd;

class ReplacerComparator extends AbstractComparator
{
    public function compare(): bool
    {
        return (bool)preg_match_all($this->item->from, $this->url);
    }

    public function getToUrl()
    {
        return preg_replace($this->item->from, $this->item->to, $this->url);
    }
}