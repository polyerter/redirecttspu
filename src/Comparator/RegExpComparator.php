<?php
declare(strict_types=1);


namespace RedirectTspu\Comparator;



class RegExpComparator  extends AbstractComparator
{
    public function compare(): bool
    {
        return (bool)preg_match_all($this->item->from, $this->url);
    }
}